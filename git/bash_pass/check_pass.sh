#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
pass=$1
# check number greater then 10 
if [[ ${#pass} -gt 10 ]] ; then
	# check capital latter in the string
	if [[ "$pass" =~ [A-Z] ]];
	then
		# check lowercase latter in the string
		if [[ "$pass" =~ [a-z] ]];
	       	then
			# check digit in the string
			if [[ $pass =~ [0-9] ]];
			then
				# check special character in the string
				if [[ $pass =~ ['!@#$%^&*()_+'] ]]; 
				then
					echo -e ${RED}"special character are forbidden"
					exit 1
				else 
					echo -e ${GREEN}"$pass"
					exit 0
				fi
			else
				echo -e ${RED}"needed to be digit in pass "
				exit 1
			fi
		
		else
			echo -e ${RED}"needed to be lowercase in pass"
   			exit 1
		fi
	else
   	echo -e ${RED}"needed to be uppercase in pass"
   	exit 1
	fi
else 
	echo -e ${RED}"pass needed to be greater then 10 characters"
	exit 1
fi
